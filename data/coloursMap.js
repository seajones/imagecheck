module.exports = [
    {
        "name": "black",
        "red": 36,
        "green": 36,
        "blue": 36
    },
    {
        "name": "grey",
        "red": 57,
        "green": 70,
        "blue": 86
    },
    {
        "name": "teal",
        "red": 0,
        "green": 98,
        "blue": 110
    },
    {
        "name": "navy",
        "red": 0,
        "green": 0,
        "blue": 79
    }
]