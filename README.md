# imageCheck
## A highly imaginatively named app for a tech test

## Introduction
This is a technical test I was given to do, the requirements of which are:
* Provide an endpoint to which an URL of a PNG image can be submitted.
* Analyse the image, and return which of a number of predefined RGB colours it most closely matches from a predefined palette
* Return the palette entry name

## Running the code
The code will be running already at a Heroku instance here https://imagecheck-tech-test.herokuapp.com/imageCheck/, but can be downloaded using the following instructions:

* Pull the repo to a local directory
* Use Node 9 or above (If you don't have Node 9 installed locally, you can either install it using the [installer](https://nodejs.org/en/download/), or - preferably for version management - you can install [Creationix's NVM](https://github.com/creationix/nvm), which will run on linux, Mac, or the Bash shell of Windows 10 and above.)
* Run the program using `node main.js`
* You should see the message 'Listening on port 3000'
* _NB: You can override the port by setting an environment variable of `PORT` to a different port number (1-65536)_

## Querying the endpoint
Once running, you can query the endpoint using this syntax:

`[PROTOCOL]://[HOSTNAME]:[PORTNUMBER]/imageCheck/[URLOFIMAGE]`

`[URLOFIMAGE]` *must* be UrlEncoded to avoid conflicts within url translation.

Once processed, the endpoint will respond with one of these statuses:

* 200 and a colour name if the colour has been matched within a reasonable similiarity
* 404 and `Could not match this image to a colour` if the colour could not be matched
* 400 if your submission was incorrect in some way (e.g. if you submitted a non-URI scheme locator)
* 500 if something goes wrong internally (typically if the image is far too huge to load etc)

## Example test points:

* [NightSky black image - should identify](https://imagecheck-tech-test.herokuapp.com/imageCheck/https%3A%2F%2Fpwintyimages.blob.core.windows.net%2Fsamples%2Fstars%2Fsample-black.png)
* [NightSky grey image - should identify](https://imagecheck-tech-test.herokuapp.com/imageCheck/https%3A%2F%2Fpwintyimages.blob.core.windows.net%2Fsamples%2Fstars%2Fsample-grey.png)
* [NightSky navy image - should identify](https://imagecheck-tech-test.herokuapp.com/imageCheck/https%3A%2F%2Fpwintyimages.blob.core.windows.net%2Fsamples%2Fstars%2Fsample-navy.png)
* [NightSky teal image - should identify](https://imagecheck-tech-test.herokuapp.com/imageCheck/https%3A%2F%2Fpwintyimages.blob.core.windows.net%2Fsamples%2Fstars%2Fsample-teal.png)
* [Testcard image - should be indeterminate colour](https://imagecheck-tech-test.herokuapp.com/imageCheck/https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fb%2Fbf%2FTest_card.png)

## See Also:

* [Problem defintion](docs/problem_definition.md)
* [Approach discussion](docs/approach_discussion.md)