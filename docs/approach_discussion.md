# How I approached this

Ultimately, this had three distinct stages to achieve:

* Generate a colour representative of the image
* Use a model to compare that to known colours, establishing distance
* Return closest match if within threshold

## Generating a colour
I took several approaches to this:
* Mean average all pixels
* Mode average all pixels (find the most commonly occurring colour)
* Look at only non-white pixels (useful for the particular sample images)
* Look at only a small area (again, specific to the images sent)
* Reduce the image down to a small size using a bicubic resize, and average those smaller number of pixels

The most successful approach was to resize the image to 10x10, which cancelled out a lot of the noise from finer details.  Taking a mean average of those 100 pixels gave a very close match to the actual background colour.

## Compare to known colours
This was a straightforward euclidean maths problem.  The shortest distance between two points is a straight line, so I had to calculate the length of the distances between each of the known colour palette, and the colour being assessed.  RGB is a 3 dimensional system, so this is equivalent to plotting them on a 3D graph, and measuring the distance.  I used a library for the calculation, but it's not complicated, and in earlier versions I had written my own.

Some complications here were caused by whether HSL or LAB would provide better matching - they didn't.  I reallised that this is because whilst colour matching is a _perceptive_ exercise, matching two colours known by a computer is a purely _mathematical_ exercise, so the euclidean distance between RGB colours was ideal, and HSL/HSV were no better, and LAB actually worse, because it is biased to human perception, which is irrelevant to this problem.

## Return closest match if within threshold
The longest possible distance between two colours is the euclidean distance between (0, 0, 0) and (255, 255, 255), which is slightly over 441.  I chose a threshold of c.1/5th of this to consider it close enough, and used 50 as my threshold.

## Comments
The ideal would be to plot all colours on a 2 dimensional graph to make the comparison simpler, and less computationally expensive, but colour maps that exist on 2 axes are perceptual, not mathematical, and so aren't as useful.

The solution is a bit slow, but this is mainly because the files are quite large, and the PNG decoder is in Javascript, when using a C++ extension would make more sense, but is harder to achieve whilst still showcasing on a free Heroku instance.  In ideal terms, this whole approach would be faster in many other languages, and could be built out of different languages for speed (a JS server accessing a binary to identify the colour etc).  Good choices might include Python, for it's speed, and ability to expose a web server trivially, and C++ for roughly the same reasons - although routing the binary through an existng server like NGinx would make more sense.