# Problem definition

## Summary
Given a PNG image, perform the following steps:
* Create a model to create a representative colour for the image
* Create a model that reliably matches that colour to a palette of defined RGB colours, with names, and return the name of the most similar colour, if the colour is suitably similar to a palette value.

## Specific problems

Colour matching is a potentially difficult problem due a few factors:

* Whilst it can be approached mathematically, it deals with colour, which is a _perceptual_ bound concept, complicating matters.
* There are multiple ways of expressing colours in a distinct way: RGB, CMYK, HSL, HSV, and various palettes of standard colours by name
* Whilst conversion between the types of colour definition exists and the are accepted methods of doing this, it's not a perfect conversion in some cases because the metrics each system measure require conversion between additive colour (RGB), subtractive colour (CMYK), and conceptual colours (CSS colour palette, Pantone), which can never be perfect.
* When matching colours, edge cases occur when using mean or modal averaging of colours in an image, whereby the produced defining colour of an image can be closer to a wrong definition than the correct one - a model must be created to reliably cope with these cases.

## Possible approaches

### Image colour derivation
* Mean average of all pixels
* Modal average of all pixels
* Filter pixels to only average certain of them (e.g. remove all white pixels to only average those with an actual colour, etc.)
* Rather than derive a colour, create a Fourier transform of the image, to plot known cookout points against.

### Colour matching

* K-nearest-neighbor comparison based on RGB
* K-nearest-neighbor comparison based on other colour expression like HSL
* Match based on step-by-step matching of nearest hue, then limits on other metrics (saturation, luminance) to ensure similarity

## Commentary

kNN approach has a marked advantage of it proves to work, as it is a machine learning staple that would allow incorrect outputs to be added as training data points that further the classification abilities of the model as it is used.

It may not be necessary to compare all factors, for example saturation or luminance may prove less useful in matching colours than hue.

Because dimensional proximity on different factors (like R, G, B or H, S, L) may be of varying importance in the model, a kNN model may need to be built with weighting on different factors.