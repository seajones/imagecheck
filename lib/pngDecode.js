const { PNG } = require('pngjs3')
const { createReadStream } = require('fs')

module.exports = function (fileUri) {
    return new Promise((resolve, reject) => {
        try {
            createReadStream(fileUri)
                .pipe(new PNG())
                .on('parsed', function () {
                    resolve(this)
                })
                .on('error', reject)
        }
        catch(error) {
            reject(error)
        }
    });
}