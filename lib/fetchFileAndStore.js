const temp = require('temp').track()
const { FetchStream } = require('fetch')

module.exports = function (uri) {
    return new Promise((resolve, reject) => {
        const fetch = new FetchStream(uri)
        const tempStream = temp.createWriteStream()

        fetch.pipe(tempStream)

        fetch.on('error', reject)
        tempStream.on('error', reject)

        tempStream.on('finish', () => {
            resolve(tempStream.path)
        });
    })
}