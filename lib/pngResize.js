const os = require('os')
const crypto = require('crypto')
const { join } = require('path')
const jimp = require('jimp')

const imageSquareSize = 10

module.exports = function (fileName) {
    return new Promise((resolve, reject) => {
        const hash = crypto.createHash('sha256')
        const filename = new Date().valueOf().toString()
        hash.update(filename)
        const writeFile = join(os.tmpdir(), `${hash.digest('hex')}.png`)
    
        jimp.read(fileName, (error, image) => {
            if (error) {
                return reject(error)
            }
    
            image.resize(imageSquareSize, imageSquareSize).write(writeFile, (error, image) => {
                    if (error) {
                        return reject(error)
                    }
                    
                    resolve(writeFile)
                })
        })
    })
}