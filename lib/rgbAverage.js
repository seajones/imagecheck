module.exports = async function (pngData) {

    const data = pngData.data
    const height = pngData.height
    const width = pngData.width

    let redCount = 0
    let greenCount = 0
    let blueCount = 0
    let totalPixels = 0

    for (let x = 0; x < width; x++) {
        for (let y = 0; y < height; y++) {
            const idx = (width * y + x) << 2;

            redCount += data[idx]
            greenCount += data[idx + 1]
            blueCount += data[idx + 2]

            totalPixels++
        }
    }

    const red = redCount / totalPixels
    const green = greenCount / totalPixels
    const blue = blueCount / totalPixels

    return { red, green, blue }
}