const distance = require('euclidean-distance')

const colourData = require('../data/coloursMap.js');
const distanceTolerance = 100

module.exports = function (testColour) {

    let lowestDistance = distance([0,0,0], [255, 255, 255])
    let foundName = null;

    colourData.forEach((paletteColour, index) => {
        const distanceFromTestColour = distance(
            [paletteColour.red, paletteColour.green, paletteColour.blue],
            [testColour.red, testColour.green, testColour.blue]
        )

        if (distanceFromTestColour < lowestDistance) {
            lowestDistance = distanceFromTestColour
            foundName = paletteColour.name
        }
    })

    if (foundName && lowestDistance <= distanceTolerance) {
        return {
            colourName: foundName,
            success: true
        }
    } else {
        return {
            colourName: null,
            success: false
        }
    }
}