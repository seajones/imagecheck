const express = require('express')
const winston = require('winston')
const { join } = require('path');
const { isUri } = require('valid-url')

const pngResize = require('./lib/pngResize')
const pngDecode = require('./lib/pngDecode')
const fetchFileAndStore = require('./lib/fetchFileAndStore')
const rgaAverage = require('./lib/rgbAverage')
const closestColour = require('./lib/rgbClosestColour')

const logFile = join(__dirname, 'logs', 'imagecheck.log')

global.logger = new winston.createLogger({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: logFile })
    ],
    level: 'verbose'
})

const app = express()

app.get('/imageCheck/:uri', (request, response) => {
    const uri = decodeURI(request.params.uri)

    if (!isUri(uri)) {
        logger.error(`URI provided doesn't meet expected form`)
        return response.sendStatus(400)
    }

    fetchFileAndStore(uri)
        .then(pngResize)
        .then(pngDecode)
        .then(rgaAverage)
        .then(closestColour)

        .then( result => {
            if (result.success) {
                response.send(result);
            } else {
                response.status(404).send('Could not match this image to a colour')
            }
        })

        .catch(error => {
            logger.error(error)
            response.sendStatus(500)
        })
        
})

app.use( (error, request, response, next) => {
    logger.error('Invalid request:', error)
    response.sendStatus(400);
})

const port = process.env.PORT || 3000;

app.listen(port);

console.log(`Listening on port ${port}`);